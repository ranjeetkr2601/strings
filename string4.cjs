function string4(obj){
    let fullName = "";
    if(typeof obj === 'object' || !!obj){
        for(let key in obj){
            if(typeof key === 'string'){
                fullName = fullName.concat(obj[key][0].toUpperCase(), 
                obj[key].substring(1).toLowerCase(), 
                " ");
            }
        }
    }
    return fullName;
}
module.exports = string4;