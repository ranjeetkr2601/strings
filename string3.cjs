function string3(string){
    if(typeof string === 'string'){
        const month = Number(string.substring(string.indexOf('/') + 1, string.lastIndexOf('/')));
        
        if(!Number.isNaN(month) && (month >= 1 && month <= 12)){
            return month;
        }
        else{
            return -1;
        }
    }
    else{
        return -1;
    }
}
module.exports = string3;