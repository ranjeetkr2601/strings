function string5(stringArray){
    let resultString = "";
    if(Array.isArray(stringArray)){
        let length = stringArray.length;
        resultString = stringArray.join(' ').concat('.');
    }
    return resultString;
}
module.exports = string5;