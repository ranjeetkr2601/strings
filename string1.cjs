function string1(string){
    if(typeof string === 'string'){
        let num = Number(string.split('$').join(""));
        if(Number.isNaN(num)){
            num = 0;
        }
        return num;
    }
    else{
        return 0;
    }
    
}
module.exports = string1;