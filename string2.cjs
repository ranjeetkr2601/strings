function string2(string){
    let splitStringArray = [];
    if(typeof string === 'string'){
        splitStringArray = string.split('.');
        for(let index = 0; index < splitStringArray.length; index++){
            let num = Number(splitStringArray[index]);
            if(!Number.isNaN(num)){
                splitStringArray[index] = num;
            }
        }
    }
    return splitStringArray;
}
module.exports = string2;